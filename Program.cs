using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace HomeWork3
{
    class Program
    {
        public class Chessboard
        {
            public int[,] Board = new int[8, 8];
            public int MoveCount = 1;
            public int recCount = 0;

            public Stack<(int, int)> History = new Stack<(int, int)>();
            public List<int[,]> LockedPointsForCell = new List<int[,]>();

            public Chessboard(int x, int y)
            {
                Board[x, y] = 1;
                History.Push((x, y));
                SetPointsListWithZeroes();
            }
            public void SetPointsListWithZeroes()
            {
                for (int i = 0; i < 64; i++)
                {
                    LockedPointsForCell.Add(new int[8, 8]);
                }
            }
            public void MovesRecursion()
            {
                var StartTimer = DateTime.Now;

                while (true)
                {
                    recCount++;
                    int[,] LP = LockedPointsForCell[MoveCount - 1];

                    if (History.Peek().Item1 - 2 >= 0 && History.Peek().Item2 + 1 <= 7 && Board[History.Peek().Item1 - 2, History.Peek().Item2 + 1] == 0 && LP[History.Peek().Item1 - 2, History.Peek().Item2 + 1] != -1)
                    {
                        MoveCount++;
                        Board[History.Peek().Item1 - 2, History.Peek().Item2 + 1] = MoveCount;
                        History.Push((History.Peek().Item1 - 2, History.Peek().Item2 + 1));
                    }
                    else if (History.Peek().Item1 - 1 >= 0 && History.Peek().Item2 + 2 <= 7 && Board[History.Peek().Item1 - 1, History.Peek().Item2 + 2] == 0 && LP[History.Peek().Item1 - 1, History.Peek().Item2 + 2] != -1)
                    {
                        MoveCount++;
                        Board[History.Peek().Item1 - 1, History.Peek().Item2 + 2] = MoveCount;
                        History.Push((History.Peek().Item1 - 1, History.Peek().Item2 + 2));
                    }
                    else if (History.Peek().Item1 + 1 <= 7 && History.Peek().Item2 + 2 <= 7 && Board[History.Peek().Item1 + 1, History.Peek().Item2 + 2] == 0 && LP[History.Peek().Item1 + 1, History.Peek().Item2 + 2] != -1)
                    {
                        MoveCount++;
                        Board[History.Peek().Item1 + 1, History.Peek().Item2 + 2] = MoveCount;
                        History.Push((History.Peek().Item1 + 1, History.Peek().Item2 + 2));
                    }
                    else if (History.Peek().Item1 + 2 <= 7 && History.Peek().Item2 + 1 <= 7 && Board[History.Peek().Item1 + 2, History.Peek().Item2 + 1] == 0 && LP[History.Peek().Item1 + 2, History.Peek().Item2 + 1] != -1)
                    {
                        MoveCount++;
                        Board[History.Peek().Item1 + 2, History.Peek().Item2 + 1] = MoveCount;
                        History.Push((History.Peek().Item1 + 2, History.Peek().Item2 + 1));
                    }
                    else if (History.Peek().Item1 + 2 <= 7 && History.Peek().Item2 - 1 >= 0 && Board[History.Peek().Item1 + 2, History.Peek().Item2 - 1] == 0 && LP[History.Peek().Item1 + 2, History.Peek().Item2 - 1] != -1)
                    {
                        MoveCount++;
                        Board[History.Peek().Item1 + 2, History.Peek().Item2 - 1] = MoveCount;
                        History.Push((History.Peek().Item1 + 2, History.Peek().Item2 - 1));
                    }
                    else if (History.Peek().Item1 + 1 <= 7 && History.Peek().Item2 - 2 >= 0 && Board[History.Peek().Item1 + 1, History.Peek().Item2 - 2] == 0 && LP[History.Peek().Item1 + 1, History.Peek().Item2 - 2] != -1)
                    {
                        MoveCount++;
                        Board[History.Peek().Item1 + 1, History.Peek().Item2 - 2] = MoveCount;
                        History.Push((History.Peek().Item1 + 1, History.Peek().Item2 - 2));
                    }
                    else if (History.Peek().Item1 - 1 >= 0 && History.Peek().Item2 - 2 >= 0 && Board[History.Peek().Item1 - 1, History.Peek().Item2 - 2] == 0 && LP[History.Peek().Item1 - 1, History.Peek().Item2 - 2] != -1)
                    {
                        MoveCount++;
                        Board[History.Peek().Item1 - 1, History.Peek().Item2 - 2] = MoveCount;
                        History.Push((History.Peek().Item1 - 1, History.Peek().Item2 - 2));
                    }
                    else if (History.Peek().Item1 - 2 >= 0 && History.Peek().Item2 - 1 >= 0 && Board[History.Peek().Item1 - 2, History.Peek().Item2 - 1] == 0 && LP[History.Peek().Item1 - 2, History.Peek().Item2 - 1] != -1)
                    {
                        MoveCount++;
                        Board[History.Peek().Item1 - 2, History.Peek().Item2 - 1] = MoveCount;
                        History.Push((History.Peek().Item1 - 2, History.Peek().Item2 - 1));
                    }
                    else if (MoveCount < 64)
                    {
                        Board[History.Peek().Item1, History.Peek().Item2] = 0;
                        LockedPointsForCell[MoveCount - 1] = new int[8, 8];
                        MoveCount--;
                        LockedPointsForCell[MoveCount - 1][History.Peek().Item1, History.Peek().Item2] = -1;
                        History.Pop();
                    }

                    if (MoveCount == 63)
                    {
                        Console.WriteLine("It's almost done! 63/64");
                        for (int i = 0; i < 8; i++)
                        {
                            for (int j = 0; j < 8; j++)
                            {
                                Console.Write(Board[i, j] + " ");
                            }
                            Console.WriteLine();
                        }
                        Console.WriteLine();
                        Console.WriteLine(recCount);
                        Console.WriteLine();
                        break;
                    }
                    Console.WriteLine("Loop count: " + recCount+ "  Move count: " + MoveCount);
                }
                Console.WriteLine(StartTimer + " - " + DateTime.Now);
            }
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Введите позицию коня по вертикали(1 - 8)");
            int a = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите позицию коня по горизонтали(1 - 8)");
            int b = int.Parse(Console.ReadLine());


            Chessboard chessboard = new Chessboard(a - 1, b - 1);

            var stackSize = 2147483647;
            Thread thread = new Thread(new ThreadStart(chessboard.MovesRecursion), stackSize);
            thread.Start();

            Console.Read();
        }
    }
}
/*
                if (x - 2 >= 0 && y + 1 <= 7 && Board[x - 2, y + 1] == 0 && LP[x - 2, y + 1] != -1)
                {
                    MoveCount++;
                    Board[x - 2, y + 1] = MoveCount;
                    History.Push((x - 2, y + 1));
                    return MovesRecursion(x - 2, y + 1);
                }
                else if (x - 1 >= 0 && y + 2 <= 7 && Board[x - 1, y + 2] == 0 && LP[x - 1, y + 2] != -1)
                {
                    MoveCount++;
                    Board[x - 1, y + 2] = MoveCount;
                    History.Push((x - 1, y + 2));
                    return MovesRecursion(x - 1, y + 2);
                }
                else if (x + 1 <= 7 && y + 2 <= 7 && Board[x + 1, y + 2] == 0 && LP[x + 1, y + 2] != -1)
                {
                    MoveCount++;
                    Board[x + 1, y + 2] = MoveCount;
                    History.Push((x + 1, y + 2));
                    return MovesRecursion(x + 1, y + 2);
                }
                else if (x + 2 <= 7 && y + 1 <= 7 && Board[x + 2, y + 1] == 0 && LP[x + 2, y + 1] != -1)
                {
                    MoveCount++;
                    Board[x + 2, y + 1] = MoveCount;
                    History.Push((x + 2, y + 1));
                    return MovesRecursion(x + 2, y + 1);
                }
                else if (x + 2 <= 7 && y - 1 >= 0 && Board[x + 2, y - 1] == 0 && LP[x + 2, y - 1] != -1)
                {
                    MoveCount++;
                    Board[x + 2, y - 1] = MoveCount;
                    History.Push((x + 2, y - 1));
                    return MovesRecursion(x + 2, y - 1);
                }
                else if (x + 1 <= 7 && y - 2 >= 0 && Board[x + 1, y - 2] == 0 && LP[x + 1, y - 2] != -1)
                {
                    MoveCount++;
                    Board[x + 1, y - 2] = MoveCount;
                    History.Push((x + 1, y - 2));
                    return MovesRecursion(x + 1, y - 2);
                }
                else if (x - 1 >= 0 && y - 2 >= 0 && Board[x - 1, y - 2] == 0 && LP[x - 1, y - 2] != -1)
                {
                    MoveCount++;
                    Board[x - 1, y - 2] = MoveCount;
                    History.Push((x - 1, y - 2));
                    return MovesRecursion(x - 1, y - 2);
                }
                else if (x - 2 >= 0 && y - 1 >= 0 && Board[x - 2, y - 1] == 0 && LP[x - 2, y - 1] != -1)
                {
                        MoveCount++;
                        Board[x - 2, y - 1] = MoveCount;
                        History.Push((x - 2, y - 1));
                        return MovesRecursion(x - 2, y - 1);
                }
                else if(MoveCount <64)
                {
                    //Сохраняет текущую точку как тупик, присваивает ей 0, возвращает в предыдущую позицию
                    Board[x, y] = 0;
                    LockedPointsForCell[MoveCount - 1] = new int[8, 8];
                    History.Pop();
                    MoveCount--;
                    LockedPointsForCell[MoveCount - 1][x,y] = -1;
                }

                //Если номер клетки 64 - выходим с рекурсии
                if (MoveCount == 63)
                {
                    Console.WriteLine("It's almost done! 62/64");
                    for (int i = 0; i < 8; i++)
                    {
                        for (int j = 0; j < 8; j++)
                        {
                            Console.Write(Board[i, j] + " ");
                        }
                        Console.WriteLine();
                    }
                    Console.WriteLine();
                    Console.WriteLine(recCount);
                    Console.WriteLine();
                    //Возвращаем готовую доску
                    return 1;
                }
                else
                {
                    //Если номер хода еще не 64, заходим в предыдущий луп рекурсии и помечаем точку как закрытую
                    return MovesRecursion(History.Peek().Item1, History.Peek().Item2);
                }                
 */
